double dividenum1(double rez1, double imz1, double rez2, double imz2)
{
    double result = 0;
    result = ((rez1 * rez2) + (imz1 * imz2)) / (rez2 * rez2 + imz2 * imz2);
    return result;

}

double dividenum2(double rez1, double imz1, double rez2, double imz2)
{
    double result = 0;
    result = ((imz1 * rez2) - (rez1 * imz2)) / (rez2 * rez2 + imz2 * imz2);
    return result;
}
