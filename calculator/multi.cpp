double multinum1(const double rez1, const double imz1, const double rez2, const double imz2)
{
    double result = 0;
    result = ((rez1 * rez2) - (imz1 * imz2));
    return result;
}

double multinum2(double rez1, double imz1, double rez2, double imz2)
{
    double result = 0;
    result = ((rez1 * imz2) + (imz1 * rez2));
    return result;
}
