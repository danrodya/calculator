#ifndef FUNCTIONS_H
#define FUNCTIONS_H
double sumnum(double a, double b);

double imagenery_sumnum(double a, double b);

double minusnum(const double c, const double d);

double imagenery_minusnum(double a, double b);

double multinum1(double rez1, double imz1, double rez2, double imz2);

double multinum2(const double rez1, const double imz1, const double rez2, const double imz2);

bool checkdivide(double rez2, double imz2);

double dividenum1(double rez1, double imz1, double rez2, double imz2);

double dividenum2(double rez1, double imz1, double rez2, double imz2);

double degreenum1(double rez1, double imz1, double rez2, double imz2, double degree);

double degreenum2(double rez1, double imz1, double rez2, double imz2, double degree);
#endif // FUNCTIONS_H
