#ifndef TST_TEST_H
#define TST_TEST_H
#include <QtCore>
#include <QtTest/QtTest>


class Test : public QObject
{
    Q_OBJECT

public:
    Test();

private slots:
    void test_sum();
    void test_minus();
    void test_multi();
};

#endif // TST_TEST_H
