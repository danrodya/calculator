

// add necessary includes here

#include <QtTest>
#include "tst_test.h"
#include <../functions.h>

// add necessary includes here

Test::Test()
{
}
void Test::test_sum()
{
    double a = 15;
    double b = 25;
    QCOMPARE(40, sumnum(a, b));
}

void Test::test_minus()
{
    const double c = 10;
    const double d = 5;
    QCOMPARE(5, minusnum(c, d));
}

void Test::test_multi()
{
    const double a = 15;
    const double b = 2;
    const double c = 13;
    const double d = 2;
    QCOMPARE(191, multinum1(a, b, c, d));
}









/*
class Test : public QObject
{
    Q_OBJECT

public:
    Test();
    ~Test();

private slots:
    void test_case1();

};

Test::Test()
{

}

Test::~Test()
{

}

void Test::test_case1()
{

}

QTEST_APPLESS_MAIN(Test)

#include "tst_test.moc"
*/
