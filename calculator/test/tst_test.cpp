#include <QtTest>
#include "tst_test.h"
#include <../functions.h>

// add necessary includes here

Test::Test()
{
}
void Test::test_sum()
{
    double a = 15;
    double b = 25;
    QCOMPARE(40, sumnum(a, b));
}

void Test::test_minus()
{
    const double c = 10;
    const double d = 5;
    QCOMPARE(5, minusnum(c, d));
}

void Test::test_multi()
{
    const double a = 15;
    const double b = 2;
    const double c = 13;
    const double d = 2;
    QCOMPARE(191, multinum1(a, b, c, d));
}

void Test::test_divide()
{
    const double a = 15;
    const double b = 2;
    const double c = 13;
    const double d = 2;
    QCOMPARE(222, dividenum1(a, b, c, d));
}

void Test::test_checkdivide()
{
    double a = 15;
    double b = 2;
    QCOMPARE(true, checkdivide(a,b));

    a = 0;
    b = 0;
    QCOMPARE(false, checkdivide(a,b));
}

void Test::test_degree()
{
    double a = 10;
    double b = 5;
    double degree = 2;
    QCOMPARE(75, degreenum1(a, b, a, b, degree));
}
