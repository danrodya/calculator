#include <QtCore>
#include <QtTest/QtTest>


class Test : public QObject
{
    Q_OBJECT

public:
    Test();

private slots:
    void test_sum();
    void test_minus();
    void test_multi();
    void test_divide();
    void test_checkdivide();
};
