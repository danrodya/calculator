double degreenum1(double rez1, double imz1, double rez2, double imz2, double degree)
{
    double result1 = 0;
    double result2 = 0;

    for(int i = 1; i < degree; i++)
     {
        result1 = ((rez1 * rez2) - (imz1 * imz2));
        result2 = ((rez1 * imz2) + (imz1 * rez2));
        rez1 = result1;
        imz1 = result2;

     }

    return result1;
}

double degreenum2(double rez1, double imz1, double rez2, double imz2, double degree)
{
    double result1 = 0;
    double result2 = 0;

    for(int i = 1; i < degree; i++)
     {
        result1 = ((rez1 * rez2) - (imz1 * imz2));
        result2 = ((rez1 * imz2) + (imz1 * rez2));
        rez1 = result1;
        imz1 = result2;

     }

    return result2;
}
